<?php

/**
 * @file
 * Contains \Drupal\webmention\Webmention.
 */

namespace Drupal\webmention;

use Drupal\Core\Entity\EntityInterface;
use IndieWeb\MentionClient;

class Webmention {

  public static function sendNotification(EntityInterface $entity) {
    $sourceURL = $entity->toUrl()->setAbsolute(TRUE);
    $targetURL = 'https://brid.gy/publish/twitter';
    $client = new MentionClient();
    $response = $client->sendWebmention($sourceURL->toUriString(), $targetURL);
  }

}
