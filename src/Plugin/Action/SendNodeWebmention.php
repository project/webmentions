<?php

namespace Drupal\webmention\Plugin\Action;

use Drupal\webmention\Webmention;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Sends a webmention for a given node.
 *
 * @Action(
 *   id = "webmention_send_node_webmention",
 *   label = @Translation("Send a webmention to brid.gy"),
 *   type = "node"
 * )
 */
class SendNodeWebmention extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(NodeInterface $entity = NULL) {
    if ($entity->isPublished()) {
      Webmention::sendNotification($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $object */
    $result = $object->access('update', $account, TRUE)
      ->andIf($object->status->access('edit', $account, TRUE));

    return $return_as_object ? $result : $result->isAllowed();
  }

}
